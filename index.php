<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BUPCVQS</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
    

<header>

    <a href="#" class="logo"><span>BUPCVQS</span></a>

    <input type="checkbox" id="menu-bar">
    <label for="menu-bar" class="fas fa-bars"></label>

    <nav class="navbar">
        <a href="#home">Home</a>
        <a href="#about">About</a>
        <a href="#how">How to Queue</a>
        <a href="countersview.php">Queue Now</a>
    </nav>

</header>

<section class="home" id="home">

    <div class="content">
        <h3>Welcome to <span>BUPCVQS</span></h3>
        <p>Experience hassle free queueing with accurate estimated wait time by using Bicol University Polangui Campus Virtual Queueing System.</p>
        <a href="countersview.php" class="btn">Queue Now</a>
    </div>

    <div class="image">
        <img src="images/undraw_queue.png" alt="">
    </div>

</section>

<section class="features" id="about" style="min-height: 60vh;">

    <h1 class="heading">Virtual Queue</h1>

    <div class="box-container">

        <div class="box">
            <img src="images/time.png" alt="">
            <h3>No Time Wasted</h3>
            <p>Long lines again? Say no more! Queue on your designated counter and do as much as you want as you wait inside or near the campus.</p>
        </div>

        <div class="box">
            <img src="images/newnotif.png" alt="">
            <h3>Realtime Notifications</h3>
            <p>Tired of wasting your time? Allow yourself to efficiently make use of your time by getting notified via SMS notification with the status of your queue.</p>
        </div>

        <div class="box">
            <img src="images/distancing.png" alt="">
            <h3>Reduced Crowding</h3>
            <p>Maintain your waiting rooms clear and keep your fellow students remotely, no more inconvenience and frustration to those waiting to be served. We'll update you when you are next to be served.</p>
        </div>

    </div>

</section>


<section class="about" id="aboutt">

    <h1 class="heading"> about the system </h1>

    <div class="column">

        <div class="image">
            <img src="images/product.png" alt="">
        </div>

        <div class="content">
            <h3>BUPC Virtual Queueing System</h3>
            <p>The BUPC Virtual Queueing system is a service-based environment that provides assistance, efficacy on handling queues and a better overall experience for the users as it places the users in a virtual waiting line or queue, and they don’t have to physically wait in line to get a service or finish a transaction.</p>
            <p>As the environment changes due to the Covid-19 pandemic situation, Universities and organizations need to adapt to the new normal. Universities practices were overruled by government guidance, policies, and regulations; and these major changes had fundamental impacts on organizational culture and practices, business operations, and students experiences.  BUPC virtual queueing system aims to redefine the usual queueing scenarios by eliminating long lines, providing contact-free queueing solutions, and managing office's operations effectively.</p>

        </div>

    </div>

</section>


<section class="review" id="how">

    <h1 class="heading"> How can I Queue? </h1>

    <div class="box-container">

        <div class="box">
            <div class="user">
                <h3>1</h3>
                <div class="comment">
                    Select the counter you want to queue on.
                </div>
            </div>
            <img src="images/counters.png" alt="" width="50%">
        </div>

        <div class="box">
            <div class="user">
                <h3>2</h3>
                <div class="comment">
                    You will now view the queues on the counter you selected.
                </div>
            </div>
            <img src="images/queues.png" alt="" width="50%">
        </div>

        <div class="box">
            <div class="user">
                <h3>3</h3>
                <div class="comment">
                    Click register under the navbar tab.
                </div>
            </div>
            <img src="images/register.png" alt="" width="100%">
        </div>      

        <div class="box">
            <div class="user">
                <h3>4</h3>
                <div class="comment">
                    Fill-up the register form and click register.
                </div>
            </div>
            <img src="images/fillup.png" alt="" width="50%">
        </div>  

        <div class="box">
            <div class="user">
                <h3>5</h3>
                <div class="comment">
                    A form will pop-up confirming your queue with your queue number.
                </div>
            </div>
            <img src="images/confirmation.png" alt="" width="50%">
        </div>       

        <div class="box">
            <div class="user">
                <h3>6</h3>
                <div class="comment">
                    You will now see the updated queue with your queue number.
                </div>
            </div>
            <img src="images/queue.png" alt="" width="50%">
        </div> 

        <div class="box">
            <div class="user">
                <h3>7</h3>
                <div class="comment">
                     Wait for the text message from the counter before proceeding to the counter.
                </div>
            </div>
            <br><br><br>
            <img src="images/not.png" alt="" width="50%">
        </div> 

        <div class="box">
            <div class="user">
                <h3>Done!</h3>
                <div class="comment">
                     Great! You just finished queueing. Simple and easy as that!
                </div>
            </div>
            <img src="images/done.png" alt="" width="50%">
            <div>
                <a href="countersview.php" class="btn">Queue Now</a>
            </div>
        </div> 

    </div>

</section>

<div class="footer">

    <div class="box-container" >

        <div class="box1">
            <h3>About Us</h3>
            <p>We are BSIT 4B students from Bicol University Polangui Campus. 
            (A.Y. 2020-2021)</p>
        </div>

        <div class="box1">
            <h3>Developers</h3>
            <p>Prince Sun Lloyd B. Roncejero</p>
            <p>John Jared V. Guadines</p>
            <p>Cedric Joss Magaan</p>
        </div>

    </div>

    <h1 class="credit"> &copy; BUPC Virtual Queueing System @ 2021</h1>

</div>



</body>
</html>